<?php

namespace Drupal\address_de\EventSubscriber;

use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds a state field and a predefined list of states for Germany.
 *
 * States are not provided by the library because they're not used for
 * addressing. However, sites might want to add them for other purposes.
 */
class AddressEventsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    $events[AddressEvents::SUBDIVISIONS][] = ['onSubdivisions'];

    return $events;
  }

  /**
   * Alters the address format for Germany.
   *
   * @param \Drupal\address\Event\AddressFormatEvent $event
   *   The address format event.
   */
  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();
    if (isset($definition['country_code']) && 'DE' == $definition['country_code']) {
      $definition['format'] = $definition['format'] . "\n%administrativeArea";
      $definition['administrative_area_type'] = AdministrativeAreaType::STATE;
      $definition['subdivision_depth'] = 1;
      $event->setDefinition($definition);
    }
  }

  /**
   * Provides the subdivisions for Germany.
   *
   * @param \Drupal\address\Event\SubdivisionsEvent $event
   *   The subdivisions event.
   */
  public function onSubdivisions(SubdivisionsEvent $event) {
    // For administrative areas $parents is an array with just the country code.
    // Otherwise it also contains the parent subdivision codes. For example,
    // if we were defining cities in California, $parents would be ['US', 'CA'].
    $parents = $event->getParents();
    if ($event->getParents() != ['DE']) {
      return;
    }

    $definitions = [
      'country_code' => $parents[0],
      'parents' => $parents,
      'subdivisions' => [
        'BW' => [
          'name' => 'Baden-Württemberg',
          'iso_code' => 'DE-BW',
        ],
        'BY' => [
          'name' => 'Bayern',
          'iso_code' => 'DE-BY',
        ],
        'BE' => [
          'name' => 'Berlin',
          'iso_code' => 'DE-BE',
        ],
        'BB' => [
          'name' => 'Brandenburg',
          'iso_code' => 'DE-BB',
        ],
        'HB' => [
          'name' => 'Bremen',
          'iso_code' => 'DE-HB',
        ],
        'HH' => [
          'name' => 'Hamburg',
          'iso_code' => 'DE-HH',
        ],
        'HE' => [
          'name' => 'Hessen',
          'iso_code' => 'DE-HE',
        ],
        'MV' => [
          'name' => 'Mecklenburg-Vorpommern',
          'iso_code' => 'DE-MV',
        ],
        'NI' => [
          'name' => 'Niedersachen',
          'iso_code' => 'DE-NI',
        ],
        'NW' => [
          'name' => 'Nordrhein-Westfalen',
          'iso_code' => 'DE-NW',
        ],
        'RP' => [
          'name' => 'Rheinland-Pfalz',
          'iso_code' => 'DE-RP',
        ],
        'SL' => [
          'name' => 'Saarland',
          'iso_code' => 'DE-SL',
        ],
        'SN' => [
          'name' => 'Sachsen',
          'iso_code' => 'DE-SN',
        ],
        'ST' => [
          'name' => 'Sachsen-Anhalt',
          'iso_code' => 'DE-ST',
        ],
        'SH' => [
          'name' => 'Schleswig-Holstein',
          'iso_code' => 'DE-SH',
        ],
        'TH' => [
          'name' => 'Thüringen',
          'iso_code' => 'DE-TH',
        ],
      ],
    ];

    $event->setDefinitions($definitions);
  }

}
